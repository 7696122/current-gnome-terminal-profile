#!/bin/bash

FNAME="$HOME/.current_gnome_profile"
gnome-terminal --save-config=$FNAME
ENTRY=`grep ProfileID < $FNAME`
rm $FNAME
TERM_PROFILE=${ENTRY#*=}
echo -n "$TERM_PROFILE"
