current-gnome-terminal-profile
==============================

This is a very small shell script whose only purpose is to look up the name of 
the current profile used in gnome-terminal.

The solution is not beatiful, if you have a better solution, please contact me!

I found a description of the script here: 
http://www.fedoraforum.org/forum/showthread.php?t=251374

Usage
-----
    ./current-gnome-terminal-profile
will print out the name (_without_ newline) on stdout of the current profile 
in the gnome terminal
